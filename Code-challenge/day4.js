// FACTORIAL

function factorial(n) {
  let result = 1;
  for (let i = n; i > 0; i--) {
    result *= i;
  }
  return result;
}

factorial(5); // 120 -> 5 * 4 * 3 * 2 * 1
console.log(factorial(5));
