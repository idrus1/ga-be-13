// REVERSE STRING

function strReverse(str) {
  let result = "";
  for (let i = str.length - 1; i >= 0; i--) { // Start, Stop, Step
    result += str[i];
  }

  return result;
}

let sol = strReverse("amri"); // "irma"
console.log(sol);
