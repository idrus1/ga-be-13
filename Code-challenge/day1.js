// Sorting Problem
// Ga boleh pakai built-in sort

function sort(arr) {
    // Validation
        // 1. Tipe Data (Number) & (String)
    // Sanitization
        // 1. Convert String to Number
    // Step
        // 1. Cari index angka terkecil di array
    // Edge-case
    // Final Output
}

sort([1, 1000, 2, 5, -1, 60, 52, null, undefined, false, "1", "10"]);

// [1, 1000, 2, 5, -1, 60, 52, "1", "10"]
// [1, 1000, 2, 5, -1, 60, 52, 1, 10]

// [-1, 1, 1, 2, 5, 10, 52, 60, 1000]
