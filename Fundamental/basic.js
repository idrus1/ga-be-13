// Function, Looping, Array & Object Methods, Conditional. Extras ES7,ES11

// Strict-type / Loose/Dynamic-type language
// High Level vs Low Level Language
// Machine < Assesmbly < Human
// V8 Engine

// Variable - var, let, const
// Declare, Assign Value

var a; // declare
a = "test"; // assign
var a = "updated test";

let b = "test b"; // cannot re-declare
b = "updated test b"; // re-assign

const c = "test c"; // cannot re-declare
// c = "updated test c" // cannot re-assign
// console.log(a)
// console.log(b)
// console.log(c)

// Variable scope
let g = "Global";

function local() {
  let l = "Local";
  // console.log(g)
}

// console.log(l)
local();

// Data types
// string
let str = "string";
// number
let numb = 1;
// Boolean
let bul = true; // false

// Data Collection
// Array - Index
let arr = [1, "str", [[false, 2]], null, "", 6, 7, 8];
// console.log(arr[2][0][1])

// Object - Key Value pairs
let obj = {
  "string dot": "str",
  numb: 1,
  bul: true,
};

let accesor = "string dot";

obj.string;
obj["string dot"]; // if extra space
obj[accesor]; // access from variable

// Conditional Statement
// if (condition) {

// }
// else if(condition2) {

// }
// else {

// }

let isRaining = true;
let day = "sunday";

if (day == "monday" && isRaining) {
  console.log("Tidak lari");
} else if (day == "sunday" && !isRaining) {
  console.log("lari");
} else {
  console.log("Aku sekolah");
}

// Ternary Operator
// condition ? true : false
// e.g. Cek jika hari minggu, print aku keluar, klo bukan, aku di rumah
day == "sunday" ? console.log("Keluar") : console.log("Di rumah");

// Operators
// + - * / % **
let strr = 1 + Number("1") + 2 + 3; // "1123"
let str1 = 1 + "Nama";
let num = 1 + 1;
let modulus = 5 % 2;
let exp = Math.pow(5, 2);
let exp2 = 5 ** 2;
// console.log(strr);
// console.log(num);
// console.log(modulus);
// console.log(exp)
// console.log(exp2)
// console.log(str1)

// Boolean Operators
// == , === , &&, ||
// equal vs strict equal
// equal (==) => check value only
// strict equal (===) => check Data Type and Value
// console.log(1 === 1);
// console.log(true && (true || false));
// console.log(true || false);
// console.log(true && false || true && false)

// Looping
// for(start;stop;step) {}
for (let i = 1; i <= 10; i++) {
//   console.log(i);
}

let arr1 = [1,2,3,4,5,6]
let i = 0
while (i < 5) {
//   console.log(arr1[i]);
  i++;
}

for (let el of arr1) {
    // console.log(el)
}