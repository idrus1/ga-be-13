function func1() {
  // function declaration - hoisting
}

let func2 = function () {
  // function expression
  a + b;
};

let sum = (a, b) => {
  let timesTen = a * 10;
  return timesTen + b;
}; // (a, b) => { return a + b }

let sum2 = (a, b) => a + b;
let sum3 = () => "test";
let sum4 = (a) => a;

let sol = sum(1, 2);
console.log(sol);
