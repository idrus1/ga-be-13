// BASIC - push, pop, unshift, shift, slice, splice
// COMPULSORY -forEach (for of), filter, map
// OPTIONAL - includes, find, flat
// EXTRA - reduce

let initArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// forEach
// initArr.forEach((el) => console.log(el));

// for (let i = 0; i < initArr.length; i++) {
//   console.log(initArr[i])
// }

// console.log(upFive)

// filter
let upFive = []; // container
for (let i = 0; i < initArr.length; i++) {
  let value = initArr[i];
  // console.log(value)
  if (value > 5) upFive.push(value);
}
// console.log(upFive);

let upFiveFilter = initArr.filter((el) => el > 5 && el % 2 == 0 && el !== 6);
let seven = initArr.find((el) => el == 7);
// console.log(upFiveFilter);
// console.log(seven);

// map
let timesFive = [];
for (let i = 0; i < initArr.length; i++) {
  let value = initArr[i] * 5;
  timesFive.push(value);
}
// console.log(timesFive);

let timesFiveMap = initArr
  .filter((el) => el < 6 && el % 2 == 0)
  .map((el) => ({
    ID: el,
    encodedId: `user${el} ${Math.random(10)}`,
  }));
// console.log(timesFiveMap);

// push, pop, unshift, shift

// mutable sm immutable
let arrTest = [1, "a", null, undefined, 2, 5];
let arrCopy = [...arrTest]; // Copy Array Properly

arrCopy[0] = 10;
// console.log(arrCopy, "-arr copy");
// console.log(arrTest, "-arr test");

arrTest.push("last value");
// console.log(arrTest);
arrTest.pop();
// console.log(arrTest);
arrTest.unshift("first value");
// console.log(arrTest);
arrTest.shift();
// console.log(arrTest);

let arrFields = ["wortel", "tomat", "kubis"];

let nama = ["wortel"];
let VegFilter = arrFields.filter((el) => el !== nama);
// console.log(VegFilter);

let arrT = [...nama];
// console.log(arrT);

let arrNumbers = [
  "user",
  2,
  "24062021",
  4,
  5,
  "146283",
  7,
  8,
  9,
  10,
  null,
  false,
];
let sum = 0;
for (let i = 0; i < arrNumbers.length; i++) {
  sum += arrNumbers[i];
}
// console.log(sum);
// truthy and falsy value in javascript
let arrSum = arrNumbers
  .filter((el) => el)
  .reduce((prev, current) => String(prev).toLowerCase() + current, 10);
// console.log(arrSum);

const arrLet = [];
// arrLet = "a";
arrLet[0] = 1;
// console.log(arrLet);
