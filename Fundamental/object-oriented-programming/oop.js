// DRY - Do not Repeat Yourself
// WET - Write Everything Twice
let { sumFunc } = require("./utils/mathHelper");

// // functional programming
// let sumFunc = function (a, b) {
//   // function declaration vs function expression
//   const taxRate = 754329857849727698345;
//   return (a + b) / taxRate;
// };

let sum = sumFunc(1, 2);
let sum2 = sumFunc(2, 3);
let sum3 = sumFunc(4, 6);

// console.log(sum);

class MathHelper {
  constructor(a, b) {
    this.num1 = a; // property
    this.num2 = b;
    this.sum = function () {
      return this.num1 + this.num2;
    }; // method
  }

  //   static division() {}

  //   static findByKTP(noKtp) {
  //       return user.ktp == noKtp
  //   }
}

// let sumObj = new MathHelper(1, 2); // instance
// MathHelper.division();
// console.log(sumObj.sum());
// console.log(sumObj instanceof MathHelper);

// let this = {
//     sum : ()
// }

// function objCreate(a, b) {
//   this.a = a;
//   this.b = b;
// }

// let objFunc = new objCreate(1, 2);
// console.log(objFunc);

// FOUR PILLARS

// Inheritance - extends, super

class Multiplication extends MathHelper {
  constructor(a, b) {
    super(a, b);
    this.multiply = function () {
      return this.num1 * this.num2;
    };
  }
}

let mult = new Multiplication(1, 2);
// console.log(num1);
console.log(mult.sum());
console.log(mult.multiply());
