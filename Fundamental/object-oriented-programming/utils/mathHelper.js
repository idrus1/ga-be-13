exports.sumFunc = function (a, b) {
  const taxRate = 754329857849727698345;
  return (a + b) / taxRate;
};
