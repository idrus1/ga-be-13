const events = require("events");
const newEmitter = new events.EventEmitter();

function createEmitter(onOpen, onClose) {
  newEmitter.once("open", onOpen);
  newEmitter.once("close", onClose);
}

let emitter = createEmitter(
  () => console.log("Opened!"),
  () => console.log("Closed!")
);

function opened(emitter) {
  newEmitter.emit("open", emitter);
}

function closed(emitter) {
  newEmitter.emit("close", emitter);
}

opened(emitter);
closed(emitter);