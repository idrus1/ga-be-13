const express = require("express");
const app = express();

let PORT = 3000;

app.get("/", (req, res) => {
  console.log("Hello World");
  res.send("Hello World");
});

app.get("/amri", (req, res) => {
  console.log("Hello Amri");
  res.send("Hello Amri");
});

app.listen(PORT, () => console.log(`Listening to PORT ${PORT}`));
